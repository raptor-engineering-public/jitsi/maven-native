Document: maven-native
Title: Debian maven-native Manual
Author: <insert document author here>
Abstract: This manual describes what maven-native is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/maven-native/maven-native.sgml.gz

Format: postscript
Files: /usr/share/doc/maven-native/maven-native.ps.gz

Format: text
Files: /usr/share/doc/maven-native/maven-native.text.gz

Format: HTML
Index: /usr/share/doc/maven-native/html/index.html
Files: /usr/share/doc/maven-native/html/*.html
